echo # This script is intended to be sourced.

sh -c "[ `ps $$ | grep bash | wc -l` -gt 0 ] || { echo 'Please switch to the bash shell before running the otsdaq-demo.'; exit; }" || exit

echo -e "setup [275]  \t ======================================================"
echo -e "setup [275]  \t Initially your products path was PRODUCTS=${PRODUCTS}"

unsetup_all

export OTSDAQ_HOME=$PWD
export PRODUCTS=$OTSDAQ_HOME/products

source ${PRODUCTS}/setup

setup mrb
setup git
source ${OTSDAQ_HOME}/localProducts_otsdaq_demo_v2_05_05_e19_s96_prof/setup

source mrbSetEnv
echo -e "setup [275]  \t Now your products path is PRODUCTS=${PRODUCTS}"
echo

# Setup environment when building with MRB (As there's no setupARTDAQOTS file)

export CETPKG_INSTALL=${PRODUCTS}
export CETPKG_J=8

export OTS_MAIN_PORT=2015

export USER_DATA="${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/BurninBoxData"
export ARTDAQ_DATABASE_URI="filesystemdb://${OTSDAQ_HOME}/srcs/otsdaq_cmsburninbox/BurninBoxDatabases/filesystemdb/2018_09_CMSBurninBox_db"
export OTSDAQ_DATA="/data/CMSBurninBox"
export PH2ACF_ROOT=${OTSDAQ_HOME}/srcs/otsdaq_cmsoutertracker/otsdaq-cmsoutertracker/Ph2_ACF
export ACFSUPERVISOR_ROOT=${OTSDAQ_HOME}/srcs/otsdaq_cmsoutertracker/otsdaq-cmsoutertracker/ACFSupervisor

##### OUTERTRACKER CONFIGURATION ##########
export PH2ACF_BASE_DIR=${OTSDAQ_HOME}/srcs/otsdaq_cmsoutertracker/otsdaq-cmsoutertracker/Ph2_ACF
export ACFSUPERVISOR_ROOT=${OTSDAQ_HOME}/srcs/otsdaq_cmsoutertracker/otsdaq-cmsoutertracker/ACFSupervisor
#	export CACTUSBIN=/opt/cactus/bin
#	export CACTUSLIB=/opt/cactus/lib
#	export CACTUSINCLUDE=/opt/cactus/include
#########
# Flags #
#########
export HttpFlag='-D__HTTP__'
export ZmqFlag='-D__ZMQ__'
export USBINSTFlag='-D__USBINST__'
export Amc13Flag='-D__AMC13__'
export AntennaFlag='-D__ANTENNA__'
export UseRootFlag='-D__USE_ROOT__'
export MultiplexingFlag='-D__MULTIPLEXING__'
export EuDaqFlag='-D__EUDAQ__'
################
# Compilations #
################

# Stand-alone application, without data streaming
#        export CompileForHerd=false
#        export CompileForShep=false

# Stand-alone application, with data streaming
export CompileForHerd=true
export CompileForShep=true

# Herd application
# export CompileForHerd=true
# export CompileForShep=false

# Shep application
# export CompileForHerd=false
# export CompileForShep=true

# Compile with EUDAQ libraries
export CompileWithEUDAQ=false

echo -e "setup [275]  \t Now your user data path is USER_DATA \t\t = ${USER_DATA}"
echo -e "setup [275]  \t Now your database path is ARTDAQ_DATABASE_URI \t = ${ARTDAQ_DATABASE_URI}"
echo -e "setup [275]  \t Now your output data path is OTSDAQ_DATA \t = ${OTSDAQ_DATA}"
echo

alias rawEventDump="art -c /home/modtest/Programming/otsdaq/srcs/otsdaq/artdaq-ots/ArtModules/fcl/rawEventDump.fcl"
alias kx='StartOTS.sh -k'

echo
echo -e "setup [275]  \t Now use 'ots -w' to configure otsdaq"
echo -e "setup [275]  \t  	Then use 'ots' to start otsdaq"
echo -e "setup [275]  \t  	Or use 'ots --help' for more options"
echo
echo -e "setup [275]  \t     use 'kx' to kill otsdaq processes"
echo

#setup ninja v1_8_2
#setup nlohmann_json v3_9_0b -q e19:prof


#setup ninja generator
#============================
ninjaver=`ups list -aKVERSION ninja|sort -V|tail -1|sed 's|"||g'`
setup ninja $ninjaver
alias mb='pushd $MRB_BUILDDIR; ninja -j$CETPKG_J; popd'
alias mbb='mrb b --generator ninja'
alias mz='mrb z; mrbsetenv; mrb b --generator ninja'
alias mt='pushd $MRB_BUILDDIR;ninja -j$CETPKG_J;CTEST_PARALLEL_LEVEL=${CETPKG_J} ninja -j$CETPKG_J test;popd'
alias mtt='mrb t --generator ninja'
alias mi='pushd $MRB_BUILDDIR; ninja -j$CETPKG_J install;popd'
alias mii='mrb i --generator ninja'




